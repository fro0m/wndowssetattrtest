#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <Windows.h>
#include <QDebug>
#include <stdio.h>
#include <iostream>
#include <string>


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
#ifdef Q_OS_WIN
    qDebug() << "it is Windows, baby";
//    std::string path {"\\?\D:\с"};
    std::string path {"C:\\c\\c.c"};
    //char path[] {"C:\Program Files (x86)\AIMP\AIMP.ini"};
    LPCSTR lstr = path.c_str();
    bool res = SetFileAttributesA(lstr, FILE_ATTRIBUTE_HIDDEN);
    std::cout << res << " " << path << "\n";
#endif

    return app.exec();
}
