#ifndef SQLITEMMODEL_H
#define SQLITEMMODEL_H

#include <QObject>
#include <QAbstractItemModel>

class SQLItemModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    SQLItemModel(QObject *parent = nullptr);
};

#endif // SQLITEMMODEL_H
